import { WebApp } from 'meteor/webapp'
import ConnectRoute from 'connect-route'

function onRoute (req, res, next) {
  if (req.url === '/') {
      // 307 Temporary Redirect
    res.writeHead(307, {
      Location: '/login'
    })
    res.end()
  } else {
      // Let other handlers match
    next()
  }
}

const middleware = ConnectRoute(function (router) {
  router.get('/', onRoute)
})

WebApp.connectHandlers.use(middleware)
