export const getUserEmail = user => user && user.services && user.services.google && user.services.google.email || user && user.emails && user.emails[0] && user.emails[0].address || ''
