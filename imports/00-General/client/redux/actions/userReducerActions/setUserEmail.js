import { SET_USER_EMAIL } from '.'

export default function setUserEmail (userEmail) {
  return {
    type: SET_USER_EMAIL,
    payload: userEmail
  }
}
