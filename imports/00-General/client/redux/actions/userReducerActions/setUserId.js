import { SET_USER_ID } from '.'

export default function setUserId (userId) {
  return {
    type: SET_USER_ID,
    payload: userId
  }
}
