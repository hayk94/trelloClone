import { SET_USER_EMAIL } from '../../actions'

export default function userEmailReducer (state = '', action) {
  switch (action.type) {
    case SET_USER_EMAIL:
      return action.payload
  }

  return state
}
