import { SET_USER_ID } from '../../actions'

export default function userIdReducer (state = '', action) {
  switch (action.type) {
    case SET_USER_ID:
      return action.payload
  }

  return state
}
