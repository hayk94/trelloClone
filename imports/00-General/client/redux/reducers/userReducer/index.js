import { combineReducers } from 'redux'

import userEmailReducer from './userEmailReducer'
import userIdReducer from './userIdReducer'

const userReducer = combineReducers({
  email: userEmailReducer,
  _id: userIdReducer
})

export default userReducer
