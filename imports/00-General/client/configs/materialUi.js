// https://material.io/color/#!/?view.left=1&view.right=0&primary.color=0D47A1&secondary.color=D50000

import { createMuiTheme } from 'material-ui/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#5472d3',
      main: '#0d47a1',
      dark: '#002171',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ff5131',
      main: '#d50000',
      dark: '#9b0000',
      contrastText: '#fff'
    }
  }
})

export default theme
