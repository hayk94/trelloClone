import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'

import { Provider, connect } from 'react-redux'
import store from './redux'

import Reboot from 'material-ui/Reboot'

import UserContainer from './UserContainer'

let test = props => <div>{JSON.stringify(props)}</div>
test = connect(state => { return {testProp: state.testReducer} })(test)

const App = props => <BrowserRouter>
  <Provider store={store}>
    <UserContainer>
      <Route path='/login' component={test} />
    </UserContainer>
  </Provider>
</BrowserRouter>

export default App
